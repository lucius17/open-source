# TP1 - systemd

- [I. `systemd`-basics](#i-systemd-basics)
  - [1. First steps](#1-first-steps)
  - [2. Gestion du temps](#2-gestion-du-temps)
  - [3. Gestion de noms](#3-gestion-de-noms)
  - [4. Gestion du réseau (et résolution de noms)](#4-gestion-du-r%c3%a9seau-et-r%c3%a9solution-de-noms)
    - [NetworkManager](#networkmanager)
    - [`systemd-networkd`](#systemd-networkd)
    - [`systemd-resolved`](#systemd-resolved)
  - [5. Gestion de sessions `logind`](#5-gestion-de-sessions-logind)
  - [6. Gestion d'unité basique (services)](#6-gestion-dunit%c3%a9-basique-services)
- [II. Boot et Logs](#ii-boot-et-logs)
- [III. Mécanismes manipulés par systemd](#iii-m%c3%a9canismes-manipul%c3%a9s-par-systemd)
  - [1. cgroups](#1-cgroups)
  - [2. D-Bus](#2-d-bus)
  - [3. Restriction et isolation](#3-restriction-et-isolation)
- [IV. systemd units in-depth](#iv-systemd-units-in-depth)
  - [1. Exploration de services existants](#1-exploration-de-services-existants)
  - [2. Création de service simple](#2-cr%c3%a9ation-de-service-simple)
  - [3. Sandboxing (heavy security)](#3-sandboxing-heavy-security)
  - [4. Event-based activation](#4-event-based-activation)
    - [A. Activation via socket UNIX](#a-activation-via-socket-unix)
    - [B. Activation automatique d'un point de montage](#b-activation-automatique-dun-point-de-montage)
    - [C. Timer `systemd`](#c-timer-systemd)
  - [5. Ordonner et grouper des services](#5-ordonner-et-grouper-des-services)




# TP Systemd

# I. `systemd`-basics

## 1. First steps

* vérifier que la version de `systemd` est > 241
  * `systemctl --version`
* 🌞 s'assurer que `systemd` est PID1
* 🌞 check tous les autres processus système (**PAS les processus kernel)
  * décrire brièvement au moins 5 autres processus système
--------------------------------------------------------------------------------------------------------------------------------
```
systemctl --version
ps -ef | grep systemd
ps -ef
```

On peut voir deux sortes de processus  :   
Ceux descendant de kthreadd, qui sont des threads du kernel et donc, comme on peut le voir avec le PPID du process kthreadd, ne descendent pas du processus systemd.  
Ils ne sont par conséquent pas traité comme des processus systèmes, ils font partie de l'espace noyau.  
Le path de ces processus apparait toujours entre crochet.

Ceux descendant de systemd (dont on peut voir le PID égal à 1, et un PPID nul), qui sont des processus système, comme :
- ModemManager, service D-Bus utilisé pour communiquer avec des modems. 
- auditd, service de montoring et de log
- chronyd, service de temps NTP
- smartd,  Service de monitoring de disque
- abrtd, une sorte de gestionnaire de bug qui collecte les problèmes de données et fait des actions appropriés 
- etc ... chaque processus peut facilement être trouvé avec une recherche internet



## 2. Gestion du temps 
La gestion du temps est désormais gérée avec `systemd` avec la commande `timedatectl`.
* `timedatectl` sans argument fournit des informations sur la machine
* 🌞 déterminer la différence entre Local Time, Universal Time et RTC time
  * expliquer dans quels cas il peut être pertinent d'utiliser le RTC time
* timezones
  * `timedatectl list-timezones` et `timedatectl set-timezone <TZ>` pour définir de timezones
  * 🌞 changer de timezone pour un autre fuseau horaire européen
* on peut activer ou désactiver l'utilisation de la syncrhonisation NTP avec `timedatectl set-ntp <BOOLEAN>`
  * 🌞 désactiver le service lié à la synchronisation du temps avec cette commande, et vérifier à la main qu'il a été coupé
--------------------------------------------------------------------------------------------------------------------------------

Local Time est le temps défini dans une certaines zones, comme un pays ou une ville, il peut donc grandement varier suivant l'endroit où on se trouve à un instant X.  
UTC est une heure définie fixe partout sur la Terre, elle sert de base de temps civil international(les différents pays s'y adapte en y ajoutant/enlevant des heures, avec par ex UTC+1).  
RTC est l'horloge interne d'une machine, qui peut différer entre toutes les machines.  

Pour lister les timezones :
```
timedatectl list-timezones
```

Pour changer de timezone :
```
timedatectl set-timezone Europe/Paris
```

Désactiver le service lité à la synchronisation du temps :
```
timedatectl set-ntp false  
```
Pour vérifier à la main l'état de ce service :
``` 
timedatectl show
```
(ou on peut aussi vérifier si le processus chronyd est bien actif)





## 3. Gestion de noms

La gestion de nom est aussi gérée par `systemd` avec `hostnamectl`.  
En réalité, `hostnamectl` peut récupérer plus d'informations que simplement le nom de domaine : `hostnamectl` sans arguments pour les voir.
* changer les noms de la machine avec `hostnamectl set-hostname`
* il est possible de changer trois noms avec `--pretty`, `--static` et `--transient`
* 🌞 expliquer la différence entre les trois types de noms. Lequel est à utiliser pour des machines de prod ?
* on peut aussi modifier certains autre noms comme le type de déploiement : `hostnamectl set-deployment <NAME>`
--------------------------------------------------------------------------------------------------------------------------------

Pour changer nom machine :
```
hostnamectl set-hostname MACHINE
```

Différence des noms de machine :
- Static : C'est le nom traditionnel que l'on peut modifier en modifier le fichier etc/hostname. Par défaut, il est égale à "localhost".
- Transient : C'est un nom d'hôte défini par le kernel. Par défaut, il est initialisé comme étant égal au nom Static. 
- Pretty : Nom mise en forme qui est utilisé pour être présenté à l'utilisateur




## 4. Gestion du réseau (et résolution de noms)
### NetworkManager
Utilisation basique en ligne de commande :
* lister les interfaces et des informations liées
  * `nmcli con show`
  * `nmcli con show <INTERFACE>`
* modifier la configuration réseau d'une interface
  * éditer le fichier de configuratation d'une interface `/etc/sysconfig/network-scripts/ifcfg-<INTERFACE_NAME>`
  * recharger NetworkManager : `sudo systemctl reload NetworkManager` (relire les fichiers de configuration)
  * redémarrer l'interface `sudo nmcli con up <INTERFACE_NAME>`
* 🌞 afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP)
--------------------------------------------------------------------------------------------------------------------------------
Pour lister les informations de toutes les interfaces:
```
nmcli con show
```
pour afficher les informations DHCP récupérées par NetworkManager (sur une interface en DHCP), ici sur l'interface enp0s3 :
```
sudo dhclient -d -nw enp0s3   
nmcli con show enp0s3 | grep -i dhcp  
```



### `systemd-networkd`

Il est aussi possible que la configuration des interfaces réseau et de la résolution de noms soit enitèrement gérée par `systemd`, à l'aide du démon `systemd-networkd`.  

Dans le cas d'utilisation de `systemd-networkd`, il est préférable de désactiver NetworkManager, afin d'éviter les conflits et l'ajout de complexité superflue :
* 🌞 stopper et désactiver le démarrage de `NetworkManager`
* 🌞 démarrer et activer le démarrage de `systemd-networkd`
--------------------------------------------------------------------------------------------------------------------------------

Stopper et désactiver NetworkManager: 
```
systemctl stop NetworkManager
systemctl disable NetworkManager
```
Démarrer et activer systemd-networkd: 
```
systemctl start systemd-networkd
systemctl enable systemd-networkd
```

La [doc officielle détaille](https://www.freedesktop.org/software/systemd/man/systemd.network.html) (**faites-y un tour**) l'ensemble des clauses possibles et peut amener à réaliser des configurations extrêmement fines et poussées : 
* configuration de zone firewall, de serveurs DNS, de serveurs NTP, etc. par interface
* utilisation des protocoles liés aux VLAN, VXLAN, IPVLAN, VRF, etc
* configuration de fonctionnalités réseau comme le bonding/teaming, bridge, MacVTap etc
* création de tunnel ou interface virtuelle
* détermination manuelle de voisins L2 (table ARP)
* etc
* 🌞 éditer la configuration d'une carte réseau de la VM avec un fichier `.network`
--------------------------------------------------------------------------------------------------------------------------------

éditer la configuration d'une carte réseau : 
```
nano /etc/systemd/network/test1.network  
```
Puis ajouter les lignes suivantes par exemple :  
[Match]  
Key=enp0s3  
[Network]  
DHCP=ipv4  
Cette config permet d'obtenir automatiquement une adresse via DHCP  
La configuration d'adresse statique n'a jamais marché pour moi malgrès de nombreuse tentative, sans doute un problème de configuration virtualbox ...  





### `systemd-resolved`
L'activation de `systemd-resolved` permet une résolution des noms de domaines avec un serveur DNS local sandboxé (sur certaines distributions c'est fait par défaut). Certains bénéfices de l'utilisation de `systemd-resolved` sont :
* configuration de DNS par interface
  * aucune requête sur des DNS potentiellement injoignables 
    * = pas de leak d'infos
    * = optimisation du temps de lookup
* résolution robuste avec un serveur DNS local sandboxé
* support natif de fonctionnalités comme DNSSEC, DNS over TLS, caching DNS

* 🌞 activer la résolution de noms par `systemd-resolved` en démarrant le service (maintenant et au boot)
  * vérifier que le service est lancé
* 🌞 vérifier qu'un serveur DNS tourne localement et écoute sur un port de l'interfce localhost (avec `ss` par exemple)
  * effectuer une commande de résolution de nom en utilisant explicitement le serveur DNS mis en place par `systemd-resolved` (avec `dig`)
  * effectuer une requête DNS avec `systemd-resolve <HOSTNAME>` ou `resolvectl query <HOSTNAME>`
* on peut utiliser `resolvectl` pour avoir des infos sur le serveur local

> `systemd-resolved` apporte beaucoup de fonctionnalités comme du caching, le support de DNSSEC ou encore de DNS over TLS. Une fois qu'il est en place, tout passe par lui, le fichier `/etc/resolv.conf est donc obsolète`

* 🌞 Afin d'activer de façon permanente ce serveur DNS, la bonne pratique est de remplacer `/etc/resolv.conf` par un lien symbolique pointant vers `/run/systemd/resolve/stub-resolv.conf`
* 🌞 Modifier la configuration de `systemd-resolved`
  * elle est dans `/etc/systemd/resolved.conf`
  * ajouter les serveurs de votre choix
  * vérifier la modification avec `resolvectl`
* 🌞 mise en place de DNS over TLS
  * renseignez-vous sur les avantages de DNS over TLS
  * effectuer une configuration globale (dans `/etc/systemd/resolved.conf`)
    * compléter la clause `DNS` pour ajouter un serveur qui supporte le DNS over TLS (on peut en trouver des listes sur internet)
    * utiliser la clause `DNSOverTLS` pour activer la fonctionnalité
      * valeur `opportunistic` pour tester les résolutions à travers TLS, et fallback sur une résolution DNS classique en cas d'erreur
      * valeur `yes` pour forcer les résolutions à travers TLS
  * prouver avec `tcpdump` que les résolutions sont bien à travers TLS (les serveurs DNS qui supportent le DNS over TLS écoutent sur le port 853)
* 🌞 activer l'utilisation de DNSSEC
--------------------------------------------------------------------------------------------------------------------------------

Pour activer systemd-resolved
```
systemctl start systemd-resolved  
systemctl enable systemd-resolved  
```
Pour vérifier qu'il est bien actif
```
systemctl status systemd-resolved  
systemd-resolved.service - Network Name Resolution  
   Loaded: loaded (/usr/lib/systemd/system/systemd-resolved.service; enabled; vend>  
   Active: active (running) since Fri 2019-11-29 16:04:25 CET; 59s ago  
```

Pour vérifier qu'un serveur DNS tourne localement
```
[lucius@lucius-pc ~]$ resolvectl
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
```
Pour envoyer une réquête DNS, ici pour résoudre le nom google.fr
```
[lucius@lucius-pc ~]$ resolvectl query google.fr
google.fr: 216.58.204.99                       -- link: wlp1s0
           2a00:1450:4007:817::2003            -- link: wlp1s0

-- Information acquired via protocol DNS in 111.7ms.
-- Data is authenticated: no
```

Pour faire le lien symbolique entre `/etc/resolv.conf` et `/run/systemd/resolve/stub-resolv.conf` pour activer de facon permanente le serveur DNS :
```
ls -sf /run/systemd/resolve/stub-resolv.conf /etc/resolv.conf
```


Pour modifier la configuration systemd-resolved, on commence par relever la conf actuelle.
```
[root@lucius-pc lucius]# resolvectl
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
  Current DNS Server: 192.168.1.1
         DNS Servers: 192.168.1.1
```
Ensuite on modifie le fichier de conf
```
nano /etc/systemd/resolved.conf 
```
Ici j'ai juste modifier le serveur dns pour celui de google (8.8.8.8)  
[Resolve]  
DNS=8.8.8.8

On redemarre le service ensuite
```
systemctl restart systemd-resolved.service 
```
Puis on vérifie que la nouvelle conf a bien été appliquée, et on voit bien que le serveur DNS a été changé
```
[root@lucius-pc lucius]# resolvectl
Global
       LLMNR setting: yes
MulticastDNS setting: yes
  DNSOverTLS setting: no
      DNSSEC setting: allow-downgrade
    DNSSEC supported: yes
         DNS Servers: 8.8.8.8
```

DNS over TLS permet de chiffrer les communications DNS avec le protocol TLS, permettant une sécurité renforcée.  
Pour l'activer, il suffit de décommentez/ajouter la ligne suivante dans le fichier de conf :  
DNSOverTLS=yes  
  
Il faut aussi trouver un serveur DNS compatible.  
Voici un site qui les référence : [site](https://dnsprivacy.org/wiki/display/DP/DNS+Privacy+Public+Resolvers)  
  
Pour activer DNSSEC, décommentez/ajouter la ligne suivante dans le fichier de conf :  
DNSSEC=true  
De base elle est censé être à DNSSEC=allow-downgrade, ce qui active le DNSSEC seulement le serveur DNS le supporte  


## 5. Gestion de sessions `logind`

Résultat commande loginctl :
```
[test@MACHINE ~]$ loginctl 
SESSION  UID USER SEAT  TTY  
      1    0 root seat0 tty1   
      3 1000 test       pts/0  
2 sessions listed.
```


## 6. Gestion d'unité basique (services)
```
systemctl list-units  
systemctl list-units -t service  
```
Les outils de l'écosystème GNU/Linux ont été modifiés pour être utilisés avec `systemd`
* par exemple `ps`
* on peut utiliser `ps` pour trouver l'unité associée à un processus donné : `ps -e -o pid,cmd,unit`
* on peut aussi effectuer un `systemctl status <PID>` qui nous retournera l'état de l'unité qui est responsable de ce PID
  * les logs sont fournis par *journald*, les stats système par les mécanismes de *cgroups* (on y revient plus tard)
* 🌞 trouver l'unité associée au processus `chronyd`
--------------------------------------------------------------------------------------------------------------------------------

```
timedatectl set-ntp true  
[test@MACHINE ~]$ ps -e -o pid,cmd,unit | grep chronyd  
   2034 /usr/sbin/chronyd           chronyd.service  
   2039 grep --color=auto chronyd   session-3.scoped  
[test@MACHINE ~]$ systemctl status 2034  
chronyd.service - NTP client/server  
On voit que chronyd correspondau service de synchronisation de temps vu plus haut.  
```


# II. Boot et Logs

* 🌞 générer un graphe de la séquence de boot
  * `systemd-analyze plot > graphe.svg`
  * très utile pour du débug
  * déterminer le temps qu'a mis `sshd.service` à démarrer
* on peut aussi utiliser `systemd-analyse blame` en ligne de commande
--------------------------------------------------------------------------------------------------------------------------------

```
systemd-analyze plot > graphe.svg  
```
![](Capture_d_écran_du_2019-11-29_16-05-05.png)  
On peut déterminer avec ce graphe que le service sshd à mis 9.5 secondes pourse lancer.  
```
systemd-analyze blame  
```


# III. Mécanismes manipulés par systemd

## 1. cgroups


```
systemd-cgls  
systemd-cgtop  
ps -e -o pid,cmd,cgroup  
cat /proc/<PID>/cgroup  
```
voir /sys/fs/cgroup  
On trouve "max", ce qui signifie que on peut allouer toute la RAM au groupe.  
```
systemctl set-property session-3.scope MemoryMax=4G  
nano /sys/fs/cgroup/user.slice/user-1000.slice/session-3.scope/memory.max  
systemctl set-property session-3.scope MemoryMax= pour la remettre à max  
ls /etc/systemd/system.control/  
```
les fichiers ne sont pas créé avec un scope (groupe de slice). Pour en faire apparaitre, il faut faire une modif sur un slice.  
```
sudo systemctl set-property sshd.service MemoryMax=2G  
```
Avec cette commande on voit bien qu'un fichier est créé dans /etc/systemd/system.control/sshd.service.d  




## 2. D-Bus
En utilisant `dbus-monitor` ou `busctl monitor`
* 🌞 observer, identifier, et expliquer complètement un évènement choisi

En utilisant D-Feet, `busctl` ou `dbus-send`
* 🌞 UPDATE : récupérer la valeur d'une propriété d'un objet D-Bus
* 🌞 envoyer un signal OU appeler une méthode, à l'aide des commandes ci-dessus
--------------------------------------------------------------------------------------------------------------------------------

J'ai choisi d'observer les variations de l'affichage sur l'état de la batterie. Il est possible d'observer des changements facilement en débranchant/rebranchant la batterie au secteur.  
Pour observer ce bus :
```
busctl monitor org.freedesktop.UPower 
```
On peut voir plusieurs différences sur les variables et leur fonction lorsqu'on débranche et lorsqu'on rebranche :  
"UpdateTime" passe de 1575560807 à  1575560825, permettant de dater chaque évenement   
"IconName" apparait seulement lorsqu'on débranche avec "battery-full-symbolic". Il permet de changer l'icone de l'état de la batterie.  
Le fait que la variable change seulement en débranchant s'explique par le fait que l'icon par défaut est sans doute "battery-full-charging-symbolic".  
"TimeToEmpty" passe 36032 à 10262. Ici je ne sais pas pourquoi la valeur descend à 10000 lorsqu'on rebranche la batterie ...  
"State" apparait seulement lorsqu'on débranche avec la valeur "2". Fonction non trouvée
"EnergyRate" passe de 2,88 à 9,933. Indique le taux de rechargement.  
"Percentage" apparait seulement lorsqu'on rebranche avec la valeur "98". Indique le pourcentage de batterie restant  
"Energy" apparait seulement lorsqu'on rebranche avec la valeur "28,317".  
"OnLine" (correspondant à brancher au secteur) passe de false à true (logique)  
"OnBattery" passe de true à false (logique)  
  

Pour récupérer la valeur d'une propriété, avec busctl, il faut récupérer le service, l'objet, l'interface et enfin le nom de la propriété voulu. Exemple :  
Avec la batterie sur secteur :
```
[root@lucius-pc lucius]# busctl get-property org.freedesktop.UPower /org/freedesktop/UPower org.freedesktop.UPower OnBattery 
b false
```

On enleve la batterie du secteur :
```
[root@lucius-pc lucius]# busctl get-property org.freedesktop.UPower /org/freedesktop/UPower org.freedesktop.UPower OnBattery 
b true
```

Malheureusement, je n'ai pas réussi à envoyer de signal sur org.freedesktop.UPower



## 3. Restriction et isolation
---
* lancer un processus sandboxé, et tracé, avec `systemd-run`
  * un shell par exemple, pour faire des tests réseaux `sudo systemd-run --wait -t /bin/bash`
  * un service est créé : `systemctl status <NAME>`
  * un cgroup est créé : `systemd-cgls` pour le repérer, puis `/sys/fs/cgroup` pour voir les restrictions appliquées
  * 🌞 identifier le cgroup utilisé
* il est aussi possible d'ajouter directement des restrictions cgroups, ou isolations namespaces
  * 🌞 lancer une nouvelle commande : ajouter des restrictions cgroups
    * `sudo systemd-run -p MemoryMax=512M <PROCESS>`
    * par exemple : `sudo systemd-run -p MemoryMax=512M --wait -t /bin/bash`
  * 🌞 lancer une nouvelle commande : ajouter un traçage réseau
    * `sudo systemd-run -p IPAccounting=true <PROCESS>`
    * par exemple : `sudo systemd-run -p IPAccounting=true --wait -t /bin/bash`
      * effectuer un ping
      * quitter le shell
      * observer les données récoltées
  * 🌞 lancer une nouvelle commande : ajouter des restrictions réseau
    * `-p IPAddressAllow=192.168.56.0/24` + `-p IPAddressDeny=any`
    * prouver que les restrictions réseau sont effectives
* `sudo systemd-nspawn --ephemeral --private-network -D / bash`
  * vérifier que `--private-network` a fonctionné : `ip a`
  * 🌞 expliquer cette ligne de commande
  * 🌞 prouver qu'un namespace réseau différent est utilisé
    * pour voir les namespaces utilisés par un processus donné, on peut aller voir dans `/proc`
    * `ls -al /proc/<PID>/ns` : montre les liens vers les namespaces utilisés (identifiés par des nombres)
    * si le nombre vu ici est différent du nombre vu pour un autre processus alors ils sont dans des namespaces différents
  * 🌞 ajouter au moins une option pour isoler encore un peu plus le processus lancé (un autre namespace par exemple)
--------------------------------------------------------------------------------------------------------------------------------

Pour lancer un processus sandboxé :
```
[root@lucius-pc lucius]# systemd-run --wait -t /bin/bash
Running as unit: run-u152.service
Press ^] three times within 1s to disconnect TTY.
[root@lucius-pc /]# systemctl status run-u152.service 
● run-u152.service - /bin/bash
   Loaded: loaded (/run/systemd/transient/run-u152.service; transient)
Transient: yes
   Active: active (running) since Thu 2019-12-05 14:09:15 UTC; 40s ago
 Main PID: 1700 (bash)
    Tasks: 3 (limit: 4915)
   Memory: 4.0M
   CGroup: /system.slice/run-u152.service
           ├─1700 /bin/bash
           ├─1743 systemctl status run-u152.service
           └─1744 less

déc. 05 14:09:15 lucius-pc systemd[1]: Started /bin/bash.
```
On voit que le cgroup utilisé est "system.slice"


Pour ajouter des restrictions cgroups :
```
[root@lucius-pc /]# systemd-run -p MemoryMax=512M --wait -t /bin/bash
Running as unit: run-u911.service
Press ^] three times within 1s to disconnect TTY.
```

Pour ajouter un traçage réseau :
```
[root@lucius-pc /]# systemd-run -p IPAccounting=true --wait -t /bin/bash
Running as unit: run-u912.service
Press ^] three times within 1s to disconnect TTY.
[root@lucius-pc /]# ping google.fr
PING google.fr(par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003)) 56 data bytes
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=1 ttl=55 time=17.8 ms
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=2 ttl=55 time=19.4 ms
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=3 ttl=55 time=20.2 ms
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=4 ttl=55 time=25.4 ms
^C
--- google.fr ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 17.768/20.689/25.425/2.867 ms
[root@lucius-pc /]# exit
Finished with result: success
Main processes terminated with: code=exited/status=0
Service runtime: 19.127s
IP traffic received: 416B
IP traffic sent: 416B
```
On voit qu'on récupère bien un nombre de données envoyés/reçus.



Pour ajouter des restrictions réseaux :
```
[root@lucius-pc ~]$ systemd-run -p IPAddressDeny=any --wait -t /bin/bash
Running as unit: run-u922.service
Press ^] three times within 1s to disconnect TTY.
```
On test un ping sur la session
```
[root@lucius-pc /]# ping google.fr
PING google.fr(par10s27-in-x03.1e100.net (2a00:1450:4007:809::2003)) 56 data bytes
^C
--- google.fr ping statistics ---
45 packets transmitted, 0 received, 100% packet loss, time 44590ms
```
On voit qu'il n'y a aucun réponse, on test un ping sur notre session qui n'a pas de restriction pour être sûr que ce n'est pas un autre problème :
```
[root@lucius-pc lucius]# ping google.fr
PING google.fr(par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003)) 56 data bytes
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=1 ttl=55 time=17.8 ms
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=2 ttl=55 time=18.2 ms
64 bytes from par21s04-in-x03.1e100.net (2a00:1450:4007:811::2003): icmp_seq=3 ttl=55 time=22.1 ms
^C
--- google.fr ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 17.843/19.379/22.139/1.955 ms
```
La restriction est donc bien effective.


Concernant l'utilisation de 'systemd-nspawn', je n'ai pas réussi à l'utiliser sur ma machine.


# IV. systemd units in-depth

## 1. Exploration de services existants

* 🌞 observer l'unité `auditd.service`
  * trouver le path où est défini le fichier `auditd.service`
  * expliquer le principe de la clause `ExecStartPost`
  * expliquer les 4 "Security Settings" dans `auditd.service`
--------------------------------------------------------------------------------------------------------------------------------

Pour observer le service auditd :
```
[root@lucius-pc lucius]# systemctl status auditd.service
● auditd.service - Security Auditing Service
   Loaded: loaded (/usr/lib/systemd/system/auditd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: man:auditd(8)
           https://github.com/linux-audit/audit-documentation
```
On peut voir le path ou est défini le fichier (/usr/lib/systemd/system/auditd.service)

La clause ExecStartPost permet de lancer une commande après les commandes effectuées avec ExecStart.



## 2. Création de service simple

🌞 Créer un fichier dans `/etc/systemd/system` qui comporte le suffixe `.service` :
* doit posséder une description
* doit lancer un serveur web
* doit ouvrir un port firewall quand il est lancé, et le fermer une fois que le service est stoppé
* doit être limité en RAM

* 🌞 UPDATE : faites en sorte que le service se lance au démarrage
  * `systemctl enable` ?...
  * expliquer ce que fait cette commande concrètement
    * la section `[Install]` des unités est nécessaire pour que `enable` fonctionne
  * vous pouvez connaître le *target* (ensemble d'unités) utilisé au boot avec `systemctl get-default`
--------------------------------------------------------------------------------------------------------------------------------

Pour créer un service :
```
nano /etc/systemd/system/test.service
```
Puis ajouter par exemple ceci :  
[Unit]  
Description=Ma description du service  
After=network.target  

[Service]  
Type=simple  
ExecStartPre=iptables -A INPUT -p tcp --dport 7777 -j ACCEPT  
ExecStart=/usr/bin/httpd -k start -DFOREGROUND  
ExecStartPost=systemctl set-property test.service MemoryMax=1G  
ExecStop=iptables -D INPUT -p tcp --dport 7777 -j ACCEPT  

[Install]  
WantedBy=multi-user.target  


  


Pour le lancer au démarrage :
```
systemctl enable test.service 
```
Cette commande sert concrètement à aller lire le fichier test.service puis effectuer les actions décritent dans celui-ci.


## 3. Sandboxing (heavy security)

Essayez d'obtenir le meilleur score avec `systemd-analyze security <SERVICE>`, en comprenant ce que vous faites.
* 🌞 Expliquer au moins 5 cinq clauses de sécurité ajoutées
  * Mettez en place au moins une mesure liée aux cgroups
    * vous pouvez vérifier que c'est le cas en regardant dans `/sys/fs/cgroup`
  * Mettez en place au moins une mesure liée aux namespaces
    * vous pouvez vérifier que c'est le cas en regardant dans `/proc/<PID>/ns`
--------------------------------------------------------------------------------------------------------------------------------



## 4. Event-based activation 

### A. Activation via socket UNIX

### B. Activation automatique d'un point de montage

### C. Timer `systemd`

## 5. Ordonner et grouper des services


# source :
https://gitlab.com/it4lik/master-opensource-2019pr
https://www.it-connect.fr/les-processus-sous-linux/
https://www.malekal.com/top-lister-processus-linux/
https://tuxicoman.jesuislibre.net/2014/04/lister-et-tuer-les-processus-en-cours-sous-linux.html
https://shapeshed.com/unix-ps/#how-to-show-a-process-hierarchy-or-tree
https://linux.die.net/man/8/abrtd
https://unix.stackexchange.com/questions/357920/what-is-different-between-static-hostname-and-icon-name-and-pretty-host-name-in
https://www.freedesktop.org/software/systemd/man/hostnamectl.html
https://www.freedesktop.org/software/systemd/man/busctl.html
https://www.linuxembedded.fr/2015/07/comprendre-dbus/
https://access.redhat.com/documentation/fr-fr/red_hat_enterprise_linux/7/html/system_administrators_guide/sect-managing_services_with_systemd-unit_files
